<?php


namespace App\Enum;


final class Language
{
    const SR = 'sr';
    const EN = 'en';
    const DE = 'de';

    private function __construct()
    { /* noop */
    }

    public static function all()
    {
        return (new \ReflectionClass(self::class))->getConstants();
    }
}