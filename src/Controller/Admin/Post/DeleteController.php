<?php


namespace App\Controller\Admin\Post;


use App\Entity\Post\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteController extends AbstractController
{
    public function delete($id){

        $em = $this->getDoctrine()->getManager();
        /**
         * @var Post $post
         */
        $post = $this->getDoctrine()->getRepository(Post::class)->findOneBy(['id' => $id]);

        try{

            $em->remove($post);
            $em->flush();
            $this->addFlash('success', 'Post successfully deleted.');
        }catch (\Exception $exception){
            $this->addFlash('error', 'An error occurred, please try again.');
        }

        return $this->redirectToRoute('admin_post_list');
    }
}