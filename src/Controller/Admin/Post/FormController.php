<?php


namespace App\Controller\Admin\Post;

use App\Entity\Post\Post;
use App\Form\Post\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    public function form(Request $request, $id = null)
    {
        $post = $this->getPost($id);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($post);
                $em->flush();

                $this->addFlash('success', 'flash.success.body');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'flash.error.body');

            }

            return $this->redirectToRoute('admin_post_form');
        }

        return $this->render('admin/post/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @return Post|object|null
     */
    public function getPost($id)
    {
        if(is_null($id))
        {
            return new Post();
        }
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        if(is_null($post))
        {
            return new Post();
        }else{
            return $post;
        }
    }
}