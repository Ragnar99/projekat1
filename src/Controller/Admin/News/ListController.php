<?php


namespace App\Controller\Admin\News;


use App\Entity\News\News;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListController extends AbstractController
{
    public function list(Request $request, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository(News::class)->buildPosts();

        $news = $paginator->paginate(
            $qb->getQuery(),
            $request->query->getInt('page', 1), 10
        );

        return $this->render('admin/news/list.html.twig', [
            'news' => $news
        ]);
    }
}