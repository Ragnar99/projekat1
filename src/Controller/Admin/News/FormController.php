<?php


namespace App\Controller\Admin\News;


use App\Entity\News\News;
use App\Form\News\NewsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    public function form(Request $request, $id = null)
    {
        $news = $this->getNews($id);

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->flush();

                $this->addFlash('success', 'flash.success.body');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'flash.error.body');

            }

            return $this->redirectToRoute('admin_post_form');
        }

        return $this->render('admin/news/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @return News|object|null
     */
    public function getNews($id)
    {
        if(is_null($id))
        {
            return new News();
        }
        $news = $this->getDoctrine()->getRepository(News::class)->find($id);
        if(is_null($news))
        {
            return new News();
        }else{
            return $news;
        }
    }

}