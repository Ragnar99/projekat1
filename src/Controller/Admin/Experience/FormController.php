<?php


namespace App\Controller\Admin\Experience;


use App\Entity\Experience\Experience;
use App\Form\Experience\ExperienceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{
    public function form(Request $request, $id = null)
    {
        $experience = $this->getExperience($id);

        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($experience);
                $em->flush();

                $this->addFlash('success', 'flash.success.body');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'flash.error.body');

            }

            return $this->redirectToRoute('admin_header_form');
        }

        return $this->render('admin/experience/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @return Experience|object|null
     */
    public function getExperience($id)
    {
        if(is_null($id))
        {
            return new Experience();
        }
        $experience = $this->getDoctrine()->getRepository(Experience::class)->find($id);
        if(is_null($experience))
        {
            return new Experience();
        }else{
            return $experience;
        }
    }
}