<?php


namespace App\Controller\Admin\Experience;


use App\Entity\Experience\Experience;
use App\Entity\Header\Header;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $experiences = $em->getRepository(Experience::class)->findAll();

        return $this->render('admin/experience/list.html.twig', [
            'experiences' => $experiences
        ]);
    }
}