<?php


namespace App\Controller\Admin\Header;


use App\Entity\Header\Header;
use App\Form\Header\HeaderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{
    public function form(Request $request, $id = null)
    {
        $header = $this->getHeader($id);

        $form = $this->createForm(HeaderType::class, $header);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($header);
                $em->flush();

                $this->addFlash('success', 'flash.success.body');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'flash.error.body');

            }

            return $this->redirectToRoute('admin_header_form');
        }

        return $this->render('admin/header/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @return Header|object|null
     */
    public function getHeader($id)
    {
        if(is_null($id))
        {
            return new Header();
        }
        $header = $this->getDoctrine()->getRepository(Header::class)->find($id);
        if(is_null($header))
        {
            return new Header();
        }else{
            return $header;
        }
    }
}