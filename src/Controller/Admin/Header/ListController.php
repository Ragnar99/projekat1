<?php


namespace App\Controller\Admin\Header;


use App\Entity\Header\Header;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListController extends AbstractController
{
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $headers = $em->getRepository(Header::class)->findAll();

        return $this->render('admin/header/list.html.twig', [
            'headers' => $headers
        ]);
    }
}