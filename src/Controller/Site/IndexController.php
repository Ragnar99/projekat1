<?php


namespace App\Controller\Site;


use App\Entity\Experience\Experience;
use App\Entity\Header\Header;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController
{
    public function index(Request $request)
    {

        $current_locale = $request->getLocale();

        $header = $this->getDoctrine()->getRepository(Header::class)->findOneBy(['language' => $current_locale]);
        $experience = $this->getDoctrine()->getRepository(Experience::class)->findOneBy(['language' => $current_locale]);

        $locales = [
            ['link' => "?_locale=sr", 'locale' => 'sr' , 'locale_current_language' => 'Serbian'],
            ['link' => "?_locale=en", 'locale' => 'en' , 'locale_current_language' => 'English'],
            ['link' => "?_locale=de", 'locale' => 'de', 'locale_current_language' => 'German']
        ];

        return $this->render('site/index.html.twig', [
            'header' => $header,
            'experience' => $experience,
            'current_locale' => $current_locale,
            'locales' =>  $locales
        ]);
    }
}