<?php


namespace App\Form\Experience;


use App\Entity\Experience\Experience;
use App\Enum\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Jezik',
                'required' => true,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (Language::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                }),
            ])
            ->add('title', TextType::class, [
                'required' => false,
                'label' => 'Naslov',
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Teks',
                'required' => false,
            ])
            ->add('item1', TextType::class, [
                'label' => 'Stavka 1',
                'required' => false,
            ])
            ->add('item2', TextType::class, [
                'label' => 'Stavka 2',
                'required' => false,
            ])
            ->add('item3', TextType::class, [
                'label' => 'Stavka 3',
                'required' => false,
            ])
            ->add('item4', TextType::class, [
                'label' => 'Stavka 4',
                'required' => false,
            ])
            ->add('attachDocument', FileType::class, [
                'required' => false,
                'label' => "Lijeva slika",
                'attr'=> ['style'=> "opacity: 1"]
            ])
            ->add('attachDocument1', FileType::class, [
                'required' => false,
                'label' => "Desna slika",
                'attr'=> ['style'=> "opacity: 1"]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Experience::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'experience_type';
    }
}