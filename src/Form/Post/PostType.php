<?php


namespace App\Form\Post;


use App\Entity\Post\Post;
use App\Enum\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Jezik',
                'required' => true,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (Language::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                }),
            ])
            ->add('title', TextType::class, [
                'required' => false,
                'label' => 'Naslov',
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Tekst',
                'required' => false,
            ])
            ->add('attachDocument', FileType::class, [
                'required' => false,
                'label' => "Slika",
                'attr'=> ['style'=> "opacity: 1"]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'post_type';
    }
}