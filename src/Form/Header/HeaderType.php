<?php


namespace App\Form\Header;


use App\Entity\Header\Header;
use App\Enum\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HeaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', ChoiceType::class, [
                'label' => 'Jezik',
                'required' => true,
                'choices'  => call_user_func(function () {
                    $keys = [];

                    foreach (Language::all() as $key) {
                        $keys[$key] = $key;
                    }
                    return $keys;
                }),
            ])
            ->add('title', TextType::class, [
                'required' => false,
                'label' => 'Naslov',
            ])
            ->add('subtitle', TextType::class, [
                'label' => 'Podnaslov',
                'required' => false,
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Tekst',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Header::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'header_type';
    }
}