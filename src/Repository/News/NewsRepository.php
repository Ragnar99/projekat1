<?php


namespace App\Repository\News;


use App\Entity\News\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @return QueryBuilder
     */
    public function buildPosts()
    {
        $qb = $this->createQueryBuilder('news')
            ->select('news')
            ->orderBy('news.id', 'DESC');
        return $qb;
    }
}