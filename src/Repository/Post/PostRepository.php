<?php


namespace App\Repository\Post;


use App\Entity\Post\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PostRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return QueryBuilder
     */
    public function buildPosts()
    {
        $qb = $this->createQueryBuilder('post')
            ->select('post')
            ->orderBy('post.id', 'DESC');
        return $qb;
    }

}