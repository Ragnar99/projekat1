<?php


namespace App\Entity\Experience;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\Experience\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Experience\ExperienceRepository")
 * @ORM\Table(name="experience")
 */
class Experience
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $item1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $item2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $item3;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $item4;

    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Experience\Image", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Type(type="App\Entity\Experience\Image",)
     * @Assert\Valid()
     */
    private $leftImage;


    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Experience\Image", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Type(type="App\Entity\Experience\Image",)
     * @Assert\Valid()
     */
    private $rightImage;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"App\Enum\Language", "all"})
     */
    private $language;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string|null
     */
    public function getItem1(): ?string
    {
        return $this->item1;
    }

    /**
     * @param string $item1
     */
    public function setItem1(string $item1): void
    {
        $this->item1 = $item1;
    }

    /**
     * @return string|null
     */
    public function getItem2(): ?string
    {
        return $this->item2;
    }

    /**
     * @param string $item2
     */
    public function setItem2(string $item2): void
    {
        $this->item2 = $item2;
    }

    /**
     * @return string|null
     */
    public function getItem3(): ?string
    {
        return $this->item3;
    }

    /**
     * @param string $item3
     */
    public function setItem3(string $item3): void
    {
        $this->item3 = $item3;
    }

    /**
     * @return string|null
     */
    public function getItem4(): ?string
    {
        return $this->item4;
    }

    /**
     * @param string $item4
     */
    public function setItem4(string $item4): void
    {
        $this->item4 = $item4;
    }

    /**
     * @return Image|null
     */
    public function getLeftImage(): ?Image
    {
        return $this->leftImage;
    }

    /**
     * @param Image $leftImage
     */
    public function setLeftImage(Image $leftImage): void
    {
        $this->leftImage = $leftImage;
    }

    /**
     * @return Image|null
     */
    public function getRightImage(): ?Image
    {
        return $this->rightImage;
    }

    /**
     * @param Image $rightImage
     */
    public function setRightImage(Image $rightImage): void
    {
        $this->rightImage = $rightImage;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return null
     */
    public function getAttachDocument()
    {
        return null;
    }

    /**
     * @param null $file
     * @return array|null
     * @throws \Exception
     */
    public function setAttachDocument($file = null)
    {
        if (!$file) return [];
        $this->attachDocument($file);
        return null;
    }

    /**
     * @param UploadedFile|null $file
     * @throws \Exception
     */
    public function attachDocument(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $image = new Image();
        $image->setFile($file);
        $this->leftImage = $image;
    }

    /**
     * @return null
     */
    public function getAttachDocument1()
    {
        return null;
    }

    /**
     * @param null $file
     * @return array|null
     * @throws \Exception
     */
    public function setAttachDocument1($file = null)
    {
        if (!$file) return [];
        $this->attachDocument1($file);
        return null;
    }

    /**
     * @param UploadedFile|null $file
     * @throws \Exception
     */
    public function attachDocument1(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $image = new Image();
        $image->setFile($file);
        $this->rightImage = $image;
    }

}