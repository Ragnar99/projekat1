<?php


namespace App\Entity\Post;

use App\Entity\Post\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Post\PostRepository")
 * @ORM\Table(name="post")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $text;

    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Post\Image", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Type(type="App\Entity\Post\Image",)
     * @Assert\Valid()
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"App\Enum\Language", "all"})
     */
    private $language;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return \App\Entity\Post\Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * @param \App\Entity\Post\Image $image
     */
    public function setImage(\App\Entity\Post\Image $image): void
    {
        $this->image = $image;
    }

    /**
     * @return null
     */
    public function getAttachDocument()
    {
        return null;
    }

    /**
     * @param null $file
     * @return array|null
     * @throws \Exception
     */
    public function setAttachDocument($file = null)
    {
        if (!$file) return [];
        $this->attachDocument($file);
        return null;
    }

    /**
     * @param UploadedFile|null $file
     * @throws \Exception
     */
    public function attachDocument(UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }
        $image = new Image();
        $image->setFile($file);
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

}