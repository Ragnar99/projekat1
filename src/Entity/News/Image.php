<?php


namespace App\Entity\News;


use App\Entity\Traits\FileTrait;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="news_image")
 *
 * @Vich\Uploadable
 */
class Image
{
    use FileTrait;
    /**
     * @var HttpFile
     *
     * @Vich\UploadableField(mapping="news_image", fileNameProperty="name", size="size")
     *
     * @Assert\File(maxSize="3M", mimeTypes={
     *     "image/jpg",
     *     "image/jpeg",
     *     "image/png",
     *     "image/gif"
     * })
     */
    private $file;
}