<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'homepage', '_controller' => 'App\\Controller\\Site\\IndexController:index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin_homepage', '_controller' => 'App\\Controller\\Admin\\IndexController:index'], null, null, null, true, false, null]],
        '/admin/post/list' => [[['_route' => 'admin_post_list', '_controller' => 'App\\Controller\\Admin\\Post\\ListController:list'], null, null, null, false, false, null]],
        '/admin/header/list' => [[['_route' => 'admin_header_list', '_controller' => 'App\\Controller\\Admin\\Header\\ListController:list'], null, null, null, false, false, null]],
        '/admin/news/list' => [[['_route' => 'admin_news_list', '_controller' => 'App\\Controller\\Admin\\News\\ListController:list'], null, null, null, false, false, null]],
        '/admin/experience/list' => [[['_route' => 'admin_experience_list', '_controller' => 'App\\Controller\\Admin\\Experience\\ListController:list'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/admin/(?'
                    .'|post/(?'
                        .'|form(?:/([^/]++))?(*:205)'
                        .'|delete/([^/]++)(*:228)'
                    .')'
                    .'|header/form(?:/([^/]++))?(*:262)'
                    .'|news/form(?:/([^/]++))?(*:293)'
                    .'|experience/form(?:/([^/]++))?(*:330)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        205 => [[['_route' => 'admin_post_form', '_controller' => 'App\\Controller\\Admin\\Post\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        228 => [[['_route' => 'admin_post_delete', '_controller' => 'App\\Controller\\Admin\\Post\\DeleteController:delete'], ['id'], null, null, false, true, null]],
        262 => [[['_route' => 'admin_header_form', '_controller' => 'App\\Controller\\Admin\\Header\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        293 => [[['_route' => 'admin_news_form', '_controller' => 'App\\Controller\\Admin\\News\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        330 => [
            [['_route' => 'admin_experience_form', '_controller' => 'App\\Controller\\Admin\\Experience\\FormController:form', 'id' => null], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
