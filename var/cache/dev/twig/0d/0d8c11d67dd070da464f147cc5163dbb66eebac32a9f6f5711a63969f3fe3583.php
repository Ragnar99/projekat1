<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_77f86a63fea08ed2f84c52f7d4dde1b967ce05812518247b9c6b94fd81748235 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 78
        echo "    </head>
    <body>
        ";
        // line 80
        $this->displayBlock('body', $context, $blocks);
        // line 81
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 123
        echo "    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <meta charset=\"utf-8\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Mega Drvo doo</title>

            <!--Favicon-->
            <link rel=\"icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/favicon/mega-drvo-72x72.png"), "html", null, true);
        echo "\" type=\"image/gif\" sizes=\"36x36\">
            <!-- <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"favicon/apple-icon-60x60.png\">
            <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"favicon/apple-icon-72x72.png\">
            <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"favicon/apple-icon-76x76.png\">
            <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"favicon/apple-icon-114x114.png\">
            <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"favicon/apple-icon-120x120.png\">
            <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"favicon/apple-icon-144x144.png\">
            <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"favicon/apple-icon-152x152.png\">
            <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"favicon/apple-icon-180x180.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"favicon/android-icon-192x192.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"favicon/favicon-32x32.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"favicon/favicon-96x96.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"favicon/favicon-16x16.png\">
            <link rel=\"manifest\" href=\"favicon/manifest.json\">
            <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
            <meta name=\"msapplication-TileImage\" content=\"/ms-icon-144x144.png\">
            <meta name=\"theme-color\" content=\"#ffffff\"> -->

            <!--    fonts-->
            <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,700,500,400,600' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,300italic' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700' rel='stylesheet' type='text/css'>

            <link href='https://fonts.googleapis.com/css?family=Alegreya:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'> -->

            <!-- Bootstrap -->
            <link href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/css/bootstrap-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/css/strock-icon.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!--    owl-carousel-->
            <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/owlcarousel/owl.carousel.css"), "html", null, true);
        echo "\">
            <link href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/css/settings.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/magnific/magnific-popup.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!--    css-->
            <link rel=\"stylesheet\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/css/style.css"), "html", null, true);
        echo "\">









";
        // line 65
        echo "
";
        // line 77
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 80
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 81
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 82
        echo "
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
            <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
            <![endif]-->

            <script src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/js/jquery-2.2.0.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <!--RS-->
            <script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/jquery.themepunch.tools.min.js"), "html", null, true);
        echo "\"></script> <!-- Revolution Slider Tools -->
            <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js"), "html", null, true);
        echo "\"></script> <!-- Revolution Slider -->
            <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.actions.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.carousel.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.migration.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.navigation.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.parallax.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/rs-plugin/js/extensions/revolution.extension.video.min.js"), "html", null, true);
        echo "\"></script>

            <script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/isotope/isotope.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/imagesloaded/imagesloaded.pkgd.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/owlcarousel/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/vendors/magnific/jquery.magnific-popup.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/js/theme.js"), "html", null, true);
        echo "\"></script>


";
        // line 121
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  318 => 121,  312 => 109,  308 => 108,  304 => 107,  300 => 106,  296 => 105,  291 => 103,  287 => 102,  283 => 101,  279 => 100,  275 => 99,  271 => 98,  267 => 97,  263 => 96,  259 => 95,  255 => 94,  251 => 93,  246 => 91,  242 => 90,  232 => 82,  222 => 81,  204 => 80,  194 => 77,  191 => 65,  178 => 50,  173 => 48,  169 => 47,  165 => 46,  160 => 44,  156 => 43,  152 => 42,  148 => 41,  118 => 14,  109 => 7,  99 => 6,  80 => 5,  68 => 123,  65 => 81,  63 => 80,  59 => 78,  57 => 6,  53 => 5,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <meta charset=\"utf-8\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Mega Drvo doo</title>

            <!--Favicon-->
            <link rel=\"icon\" href=\"{{ asset('template/favicon/mega-drvo-72x72.png')}}\" type=\"image/gif\" sizes=\"36x36\">
            <!-- <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"favicon/apple-icon-60x60.png\">
            <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"favicon/apple-icon-72x72.png\">
            <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"favicon/apple-icon-76x76.png\">
            <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"favicon/apple-icon-114x114.png\">
            <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"favicon/apple-icon-120x120.png\">
            <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"favicon/apple-icon-144x144.png\">
            <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"favicon/apple-icon-152x152.png\">
            <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"favicon/apple-icon-180x180.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"favicon/android-icon-192x192.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"favicon/favicon-32x32.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"favicon/favicon-96x96.png\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"favicon/favicon-16x16.png\">
            <link rel=\"manifest\" href=\"favicon/manifest.json\">
            <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
            <meta name=\"msapplication-TileImage\" content=\"/ms-icon-144x144.png\">
            <meta name=\"theme-color\" content=\"#ffffff\"> -->

            <!--    fonts-->
            <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,700,500,400,600' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,300italic' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700' rel='stylesheet' type='text/css'>

            <link href='https://fonts.googleapis.com/css?family=Alegreya:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'> -->

            <!-- Bootstrap -->
            <link href=\"{{ asset('template/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
            <link href=\"{{ asset('template/css/bootstrap-theme.min.css')}}\" rel=\"stylesheet\">
            <link href=\"{{ asset('template/css/font-awesome.min.css')}}\" rel=\"stylesheet\">
            <link href=\"{{ asset('template/css/strock-icon.css')}}\" rel=\"stylesheet\">
            <!--    owl-carousel-->
            <link rel=\"stylesheet\" href=\"{{ asset('template/vendors/owlcarousel/owl.carousel.css')}}\">
            <link href=\"{{ asset('template/vendors/rs-plugin/css/settings.css')}}\" rel=\"stylesheet\">
            <link href=\"{{ asset('template/vendors/magnific/magnific-popup.css')}}\" rel=\"stylesheet\">
            <!--    css-->
            <link rel=\"stylesheet\" href=\"{{ asset('template/css/style.css')}}\">









{#            #}
{#            <!-- Google Font -->#}
{#            <link href=\"https://fonts.googleapis.com/css?family=Taviraj:300,400,500,600,700,800,900&display=swap\"#}
{#                  rel=\"stylesheet\">#}
{#            <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap\" rel=\"stylesheet\">#}

{#            <!-- Css Styles -->#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/bootstrap.min.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/font-awesome.min.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/flaticon.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/linearicons.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/owl.carousel.min.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/jquery-ui.min.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/nice-select.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/magnific-popup.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/slicknav.min.css')}}\" type=\"text/css\">#}
{#            <link rel=\"stylesheet\" href=\"{{ asset('template/css/style.css')}}\" type=\"text/css\">#}
        {% endblock %}
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
            <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
            <![endif]-->

            <script src=\"{{ asset('template/js/jquery-2.2.0.min.js')}}\"></script>
            <script src=\"{{ asset('template/js/bootstrap.min.js')}}\"></script>
            <!--RS-->
            <script src=\"{{ asset('template/vendors/rs-plugin/js/jquery.themepunch.tools.min.js')}}\"></script> <!-- Revolution Slider Tools -->
            <script src=\"{{ asset('template/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js')}}\"></script> <!-- Revolution Slider -->
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.actions.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.carousel.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.kenburn.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.migration.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.navigation.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.parallax.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.slideanims.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/rs-plugin/js/extensions/revolution.extension.video.min.js')}}\"></script>

            <script src=\"{{ asset('template/vendors/isotope/isotope.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/imagesloaded/imagesloaded.pkgd.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/owlcarousel/owl.carousel.min.js')}}\"></script>
            <script src=\"{{ asset('template/vendors/magnific/jquery.magnific-popup.min.js')}}\"></script>
            <script src=\"{{ asset('template/js/theme.js')}}\"></script>


{#            <!-- Js Plugins -->#}
{#            <script src=\"{{ asset('template/js/jquery-3.3.1.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/bootstrap.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/jquery.magnific-popup.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/jquery-ui.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/jquery.nice-select.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/jquery.slicknav.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/owl.carousel.min.js') }}\"></script>#}
{#            <script src=\"{{ asset('template/js/main.js') }}\"></script>#}

        {% endblock %}
    </body>
</html>
", "base.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/base.html.twig");
    }
}
