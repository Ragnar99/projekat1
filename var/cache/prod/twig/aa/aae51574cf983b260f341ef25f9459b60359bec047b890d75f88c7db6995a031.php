<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/experience/list.html.twig */
class __TwigTemplate_0d84742e5d164574e5b52ef733d3818958755e35aa3b298d9d51047c97579ac5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin_base.html.twig", "admin/experience/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Lijeva slika</th>
                        <th scope=\"col\">Desna slika</th>
                        <th scope=\"col\">Detalji</th>
                        <th scope=\"col\">Izmjeni</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["experiences"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
            // line 19
            echo "                        <tr>
                            <td width=\"20%\">
                                ";
            // line 21
            $context["picture"] = twig_get_attribute($this->env, $this->source, $context["experience"], "leftImage", [], "any", false, false, false, 21);
            // line 22
            echo "                                ";
            $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["experience"], "leftImage", [], "any", false, false, false, 22), "file");
            // line 23
            echo "                                <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture"] ?? null), "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture"] ?? null), "id", [], "any", false, false, false, 25), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, ($context["imageSrc"] ?? null), "html", null, true);
            echo "\"/>
                                    </div>
                                </div>
                            </td>

                            <td width=\"20%\">
                                ";
            // line 31
            $context["picture1"] = twig_get_attribute($this->env, $this->source, $context["experience"], "rightImage", [], "any", false, false, false, 31);
            // line 32
            echo "                                ";
            $context["imageSrc1"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["experience"], "rightImage", [], "any", false, false, false, 32), "file");
            // line 33
            echo "                                <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture1"] ?? null), "id", [], "any", false, false, false, 33), "html", null, true);
            echo "\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture1"] ?? null), "id", [], "any", false, false, false, 35), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, ($context["imageSrc1"] ?? null), "html", null, true);
            echo "\"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: ";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "language", [], "any", false, false, false, 42), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: ";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "title", [], "any", false, false, false, 47), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "text", [], "any", false, false, false, 52), "html", null, true);
            echo "
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a class=\"btn btn-danger btn-sm\"
                                       href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_experience_form", ["id" => twig_get_attribute($this->env, $this->source, $context["experience"], "id", [], "any", false, false, false, 59)]), "html", null, true);
            echo "\">
                                        Izmjeni
                                    </a>
                                </div>
                            </td>
                        </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                    </tbody>
                </table>
            </div>
        </div>

    </div>


";
    }

    public function getTemplateName()
    {
        return "admin/experience/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 67,  145 => 59,  135 => 52,  127 => 47,  119 => 42,  107 => 35,  101 => 33,  98 => 32,  96 => 31,  85 => 25,  79 => 23,  76 => 22,  74 => 21,  70 => 19,  66 => 18,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/experience/list.html.twig", "/home/stefan/Desktop/mojiProjekti/projekat1/templates/admin/experience/list.html.twig");
    }
}
