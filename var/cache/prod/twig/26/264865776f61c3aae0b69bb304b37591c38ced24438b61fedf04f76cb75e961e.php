<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* LuneticsLocaleBundle:Switcher:switcher_links.html.twig */
class __TwigTemplate_94d365f20dcb1c088ec3eac6d935bd96976f0d96b889dc2c93abdedc01196d8f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p>Switch to :</p>
<ul class=\"locale_chooser\">
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locales"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 4
            echo "        <li";
            if ((($context["current_locale"] ?? null) == twig_get_attribute($this->env, $this->source, $context["locale"], "locale", [], "any", false, false, false, 4))) {
                echo " class=\"locale_current\"";
            }
            echo ">
            <a href=\"";
            // line 5
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "link", [], "any", false, false, false, 5), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "locale_current_language", [], "any", false, false, false, 5), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "locale_current_language", [], "any", false, false, false, 5), "html", null, true);
            echo "</a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "LuneticsLocaleBundle:Switcher:switcher_links.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 8,  52 => 5,  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "LuneticsLocaleBundle:Switcher:switcher_links.html.twig", "/home/stefan/Desktop/mojiProjekti/projekat1/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/Resources/views/Switcher/switcher_links.html.twig");
    }
}
