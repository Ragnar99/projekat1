<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/contact_process.php */
class __TwigTemplate_caaca8fba26f29dde4ab1e1fcde6be2c06a900aa5a3bca4df5499532283c6ac2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php

    \$to = \"vinodpal06@rediffmail.com\";
    \$from = \$_REQUEST['yourname'];
    \$name = \$_REQUEST['youremail'];
    \$headers = \"From: \$from\";
    \$subject = \"You have a message from your OnePro.\";

    \$fields = array();
    \$fields{\"yourname\"} = \"name\";
    \$fields{\"youremail\"} = \"email\";
    \$fields{\"subject\"} = \"subject\";
    \$fields{\"message\"} = \"message\";

    \$body = \"Here is what was sent:\\n\\n\"; foreach(\$fields as \$a => \$b){   \$body .= sprintf(\"%20s: %s\\n\",\$b,\$_REQUEST[\$a]); }

    \$send = mail(\$to, \$subject, \$body, \$headers);

?>";
    }

    public function getTemplateName()
    {
        return "site/contact_process.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "site/contact_process.php", "/home/stefan/Desktop/mojiProjekti/projekat1/templates/site/contact_process.php");
    }
}
