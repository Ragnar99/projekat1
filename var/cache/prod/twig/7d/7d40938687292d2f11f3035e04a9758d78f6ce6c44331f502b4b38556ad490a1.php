<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/news/list.html.twig */
class __TwigTemplate_13d07c533576e9d10ce1bad774b23bf93eb8736e734ce0af3f0a8510b774d1c5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin_base.html.twig", "admin/news/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Slika</th>
                        <th scope=\"col\">Detalji</th>
                        <th scope=\"col\">Akcije</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["news"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
            // line 18
            echo "                        <tr>
                            <td width=\"30%\">

                                ";
            // line 21
            $context["picture"] = twig_get_attribute($this->env, $this->source, $context["new"], "image", [], "any", false, false, false, 21);
            // line 22
            echo "                                ";
            $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["new"], "image", [], "any", false, false, false, 22), "file");
            // line 23
            echo "                                <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture"] ?? null), "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                                    <div class=\"image\">
                                        <img width=\"250\" id=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["picture"] ?? null), "id", [], "any", false, false, false, 25), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, ($context["imageSrc"] ?? null), "html", null, true);
            echo "\"/>
                                    </div>
                                </div>

                            </td>

                            <td width=\"45%\">
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["new"], "language", [], "any", false, false, false, 34), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: ";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["new"], "title", [], "any", false, false, false, 39), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: ";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["new"], "text", [], "any", false, false, false, 44), "html", null, true);
            echo "
                                    </div>
                                </div>
                            </td>
                            <td>
";
            // line 66
            echo "                            </td>
                        </tr>


                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                    </tbody>
                </table>
            </div>
        </div>


        <div class=\"clearfix\"></div>

        <ul class=\"pagination\">
            ";
        // line 80
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["news"] ?? null));
        echo "
        </ul>
    </div>


";
    }

    public function getTemplateName()
    {
        return "admin/news/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 80,  133 => 71,  123 => 66,  115 => 44,  107 => 39,  99 => 34,  85 => 25,  79 => 23,  76 => 22,  74 => 21,  69 => 18,  65 => 17,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/news/list.html.twig", "/home/stefan/Desktop/mojiProjekti/projekat1/templates/admin/news/list.html.twig");
    }
}
