<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'validator.lunetics_locale.locale' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/validator/ConstraintValidatorInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/validator/ConstraintValidator.php';
include_once $this->targetDirs[3].'/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/Validator/LocaleValidator.php';

return $this->privates['validator.lunetics_locale.locale'] = new \Lunetics\LocaleBundle\Validator\LocaleValidator(false, $this->parameters['lunetics_locale.intl_extension_fallback.iso3166'], $this->parameters['lunetics_locale.intl_extension_fallback.iso639'], $this->parameters['lunetics_locale.intl_extension_fallback.script']);
