<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'homepage', '_controller' => 'App\\Controller\\Site\\IndexController:index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin_homepage', '_controller' => 'App\\Controller\\Admin\\IndexController:index'], null, null, null, true, false, null]],
        '/admin/post/list' => [[['_route' => 'admin_post_list', '_controller' => 'App\\Controller\\Admin\\Post\\ListController:list'], null, null, null, false, false, null]],
        '/admin/header/list' => [[['_route' => 'admin_header_list', '_controller' => 'App\\Controller\\Admin\\Header\\ListController:list'], null, null, null, false, false, null]],
        '/admin/news/list' => [[['_route' => 'admin_news_list', '_controller' => 'App\\Controller\\Admin\\News\\ListController:list'], null, null, null, false, false, null]],
        '/admin/experience/list' => [[['_route' => 'admin_experience_list', '_controller' => 'App\\Controller\\Admin\\Experience\\ListController:list'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/admin/(?'
                    .'|post/(?'
                        .'|form(?:/([^/]++))?(*:43)'
                        .'|delete/([^/]++)(*:65)'
                    .')'
                    .'|header/form(?:/([^/]++))?(*:98)'
                    .'|news/form(?:/([^/]++))?(*:128)'
                    .'|experience/form(?:/([^/]++))?(*:165)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        43 => [[['_route' => 'admin_post_form', '_controller' => 'App\\Controller\\Admin\\Post\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        65 => [[['_route' => 'admin_post_delete', '_controller' => 'App\\Controller\\Admin\\Post\\DeleteController:delete'], ['id'], null, null, false, true, null]],
        98 => [[['_route' => 'admin_header_form', '_controller' => 'App\\Controller\\Admin\\Header\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        128 => [[['_route' => 'admin_news_form', '_controller' => 'App\\Controller\\Admin\\News\\FormController:form', 'id' => null], ['id'], null, null, false, true, null]],
        165 => [
            [['_route' => 'admin_experience_form', '_controller' => 'App\\Controller\\Admin\\Experience\\FormController:form', 'id' => null], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
