<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/header/list.html.twig */
class __TwigTemplate_80354f9b07951c6331a01f9ec7d2ee288b9856076eb87ccf155fdb5a63e9e916 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/header/list.html.twig"));

        $this->parent = $this->loadTemplate("admin_base.html.twig", "admin/header/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Naslovi</th>
                        <th scope=\"col\">Tekst</th>
                        <th scope=\"col\">Izmjeni</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["headers"]) || array_key_exists("headers", $context) ? $context["headers"] : (function () { throw new RuntimeError('Variable "headers" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
            // line 18
            echo "                        <tr>
                            <td width=\"30%\">
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: ";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["header"], "language", [], "any", false, false, false, 22), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 27), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Podnaslov: ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["header"], "subtitle", [], "any", false, false, false, 32), "html", null, true);
            echo "
                                    </div>
                                </div>

                            </td>

                            <td width=\"45%\">
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: ";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["header"], "text", [], "any", false, false, false, 41), "html", null, true);
            echo "
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                                                    <a class=\"btn btn-danger btn-sm\"
                                                                       href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_header_form", ["id" => twig_get_attribute($this->env, $this->source, $context["header"], "id", [], "any", false, false, false, 48)]), "html", null, true);
            echo "\">
                                                                        Izmjeni
                                                                    </a>
                                </div>
                            </td>
                        </tr>


                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                    </tbody>
                </table>
            </div>
        </div>

    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/header/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 57,  122 => 48,  112 => 41,  100 => 32,  92 => 27,  84 => 22,  78 => 18,  74 => 17,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin_base.html.twig' %}

{% block body %}

    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Naslovi</th>
                        <th scope=\"col\">Tekst</th>
                        <th scope=\"col\">Izmjeni</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for header in headers %}
                        <tr>
                            <td width=\"30%\">
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: {{ header.language }}
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: {{ header.title }}
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Podnaslov: {{ header.subtitle }}
                                    </div>
                                </div>

                            </td>

                            <td width=\"45%\">
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: {{ header.text }}
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                                                    <a class=\"btn btn-danger btn-sm\"
                                                                       href=\"{{ path('admin_header_form', {id: header.id}) }}\">
                                                                        Izmjeni
                                                                    </a>
                                </div>
                            </td>
                        </tr>


                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>

    </div>


{% endblock %}", "admin/header/list.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/admin/header/list.html.twig");
    }
}
