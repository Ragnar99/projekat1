<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/experience/list.html.twig */
class __TwigTemplate_eab61061db92c47406597551a7ac2e0e7176683ad10ea42bc5743f91c7b29868 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/experience/list.html.twig"));

        $this->parent = $this->loadTemplate("admin_base.html.twig", "admin/experience/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Lijeva slika</th>
                        <th scope=\"col\">Desna slika</th>
                        <th scope=\"col\">Detalji</th>
                        <th scope=\"col\">Izmjeni</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["experiences"]) || array_key_exists("experiences", $context) ? $context["experiences"] : (function () { throw new RuntimeError('Variable "experiences" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
            // line 19
            echo "                        <tr>
                            <td width=\"20%\">
                                ";
            // line 21
            $context["picture"] = twig_get_attribute($this->env, $this->source, $context["experience"], "leftImage", [], "any", false, false, false, 21);
            // line 22
            echo "                                ";
            $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["experience"], "leftImage", [], "any", false, false, false, 22), "file");
            // line 23
            echo "                                <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture"]) || array_key_exists("picture", $context) ? $context["picture"] : (function () { throw new RuntimeError('Variable "picture" does not exist.', 23, $this->source); })()), "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture"]) || array_key_exists("picture", $context) ? $context["picture"] : (function () { throw new RuntimeError('Variable "picture" does not exist.', 25, $this->source); })()), "id", [], "any", false, false, false, 25), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["imageSrc"]) || array_key_exists("imageSrc", $context) ? $context["imageSrc"] : (function () { throw new RuntimeError('Variable "imageSrc" does not exist.', 25, $this->source); })()), "html", null, true);
            echo "\"/>
                                    </div>
                                </div>
                            </td>

                            <td width=\"20%\">
                                ";
            // line 31
            $context["picture1"] = twig_get_attribute($this->env, $this->source, $context["experience"], "rightImage", [], "any", false, false, false, 31);
            // line 32
            echo "                                ";
            $context["imageSrc1"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["experience"], "rightImage", [], "any", false, false, false, 32), "file");
            // line 33
            echo "                                <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture1"]) || array_key_exists("picture1", $context) ? $context["picture1"] : (function () { throw new RuntimeError('Variable "picture1" does not exist.', 33, $this->source); })()), "id", [], "any", false, false, false, 33), "html", null, true);
            echo "\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture1"]) || array_key_exists("picture1", $context) ? $context["picture1"] : (function () { throw new RuntimeError('Variable "picture1" does not exist.', 35, $this->source); })()), "id", [], "any", false, false, false, 35), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["imageSrc1"]) || array_key_exists("imageSrc1", $context) ? $context["imageSrc1"] : (function () { throw new RuntimeError('Variable "imageSrc1" does not exist.', 35, $this->source); })()), "html", null, true);
            echo "\"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: ";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "language", [], "any", false, false, false, 42), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: ";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "title", [], "any", false, false, false, 47), "html", null, true);
            echo "
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "text", [], "any", false, false, false, 52), "html", null, true);
            echo "
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a class=\"btn btn-danger btn-sm\"
                                       href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_experience_form", ["id" => twig_get_attribute($this->env, $this->source, $context["experience"], "id", [], "any", false, false, false, 59)]), "html", null, true);
            echo "\">
                                        Izmjeni
                                    </a>
                                </div>
                            </td>
                        </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                    </tbody>
                </table>
            </div>
        </div>

    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/experience/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 67,  154 => 59,  144 => 52,  136 => 47,  128 => 42,  116 => 35,  110 => 33,  107 => 32,  105 => 31,  94 => 25,  88 => 23,  85 => 22,  83 => 21,  79 => 19,  75 => 18,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin_base.html.twig' %}

{% block body %}

    <div class=\"col-12 grid-margin stretch-card\">
        <div class=\"card\">
            <div class=\"table-responsive\">
                <table class=\"table table-striped\">
                    <thead>
                    <tr>
                        <th scope=\"col\">Lijeva slika</th>
                        <th scope=\"col\">Desna slika</th>
                        <th scope=\"col\">Detalji</th>
                        <th scope=\"col\">Izmjeni</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for experience in experiences %}
                        <tr>
                            <td width=\"20%\">
                                {% set picture = experience.leftImage %}
                                {% set imageSrc = vich_uploader_asset(experience.leftImage, 'file') %}
                                <div id=\"item_{{ picture.id }}\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"{{ picture.id }}\" src=\"{{ imageSrc }}\"/>
                                    </div>
                                </div>
                            </td>

                            <td width=\"20%\">
                                {% set picture1 = experience.rightImage %}
                                {% set imageSrc1 = vich_uploader_asset(experience.rightImage, 'file') %}
                                <div id=\"item_{{ picture1.id }}\">
                                    <div class=\"image\">
                                        <img width=\"200\" id=\"{{ picture1.id }}\" src=\"{{ imageSrc1 }}\"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Jezik: {{ experience.language }}
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Naslov: {{ experience.title }}
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-12\">
                                        Tekst: {{ experience.text }}
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a class=\"btn btn-danger btn-sm\"
                                       href=\"{{ path('admin_experience_form', {id: experience.id}) }}\">
                                        Izmjeni
                                    </a>
                                </div>
                            </td>
                        </tr>

                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>

    </div>


{% endblock %}", "admin/experience/list.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/admin/experience/list.html.twig");
    }
}
