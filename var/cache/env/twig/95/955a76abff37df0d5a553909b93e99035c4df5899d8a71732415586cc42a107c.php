<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/index.html.twig */
class __TwigTemplate_7a6af6104ca5b373401f22f3334b8b8e211aec992e55e4aab3d949b2e8e9bdff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "site/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 59
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 60
        echo "
<header class=\"row header navbar-static-top\" id=\"main_navbar\">
    <div class=\"container\">
        <div class=\"row m0 social-info\">
            <ul class=\"social-icon\">
                <li><a href=\"https://www.facebook.com/doomegadrvo/\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>
                <!-- <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li> -->
                <!-- <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li> -->
                <li><a href=\"https://www.instagram.com/megadrvo/\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>
                <li class=\"tel\"><a href=\"tel:+1234567890\"><i class=\"fa fa-phone\"></i> +387 55 255 188 </a></li>
                <li class=\"tel\"><a href=\"tel:+1234567890\"><i class=\"fa fa-phone\"></i> +387 55 256 032 </a></li>
                <li class=\"email\"><a href=\"#\"><i class=\"fa fa-envelope-o\"></i> megadrvo@gmail.com</a></li>
            </ul>
        </div>
    </div>
   <div class=\"logo_part\">
        <div class=\"logo\">
            <a href=\"index.html.twig\" class=\"brand_logo md-logo\">
                <img src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/header/18a.png"), "html", null, true);
        echo "\" alt=\"logo image\">
            </a>
        </div>
    </div>
    <div class=\"main-menu\">
        <nav class=\"navbar navbar-default\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#main_nav\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            
            <div class=\"menu row m0\">
                <div class=\"collapse navbar-collapse\" id=\"main_nav\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"index.html.twig\">Home</a></li>
                        <li><a href=\"#\">About Us</a></li>
                        <li><a href=\"#\">Services</a></li>
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">services</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"service.html\">services</a></li>
                                <li><a href=\"services-2.html\">services 2</a></li>
                            </ul>
                        </li> -->
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Our Products</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"#\">product</a></li>
                                <li><a href=\"#\">product 2</a></li>
                                <li><a href=\"#\">product 3</a></li>
                                <li><a href=\"#\">product details</a></li>
                            </ul>
                        </li>
                        <li><a href=\"#\">team</a></li>
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">blog</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"blog.html\">blog</a></li>
                                <li><a href=\"blog-details.html\">blog details</a></li>
                            </ul>
                        </li> -->
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Shop</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"shop.html\">Shop</a></li>
                                <li><a href=\"product.html\">Product Details</a></li>
                                <li><a href=\"cart.html\">Cart Page</a></li>
                                <li><a href=\"checkout.html\">Checkout</a></li>
                            </ul>
                        </li> -->
                        <li><a href=\"#\">COR</a></li>
                        <li><a href=\"#\">contact us</a></li>
                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle text-white\" href=\"#\" id=\"navbarDropdown\" role=\"button\"
                               data-toggle=\"dropdown\"
                               aria-haspopup=\"true\"
                               aria-expanded=\"false\">

                                ";
        // line 140
        $context["loc"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 140, $this->source); })()), "request", [], "any", false, false, false, 140), "getLocale", [], "method", false, false, false, 140);
        // line 141
        echo "
                                ";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("locale_switcher." . (isset($context["loc"]) || array_key_exists("loc", $context) ? $context["loc"] : (function () { throw new RuntimeError('Variable "loc" does not exist.', 142, $this->source); })()))), "html", null, true);
        echo "
                            </a>
                            <div class=\"dropdown-menu dropdown-menu-right animated bounceInDown\"
                                 aria-labelledby=\"navbarDropdown2\">
                                ";
        // line 146
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["locales"]) || array_key_exists("locales", $context) ? $context["locales"] : (function () { throw new RuntimeError('Variable "locales" does not exist.', 146, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 147
            echo "                                    <a class=\"dropdown-item language\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "link", [], "any", false, false, false, 147), "html", null, true);
            echo "\"
                                       title=\"";
            // line 148
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "locale_current_language", [], "any", false, false, false, 148), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("locale_switcher." . twig_get_attribute($this->env, $this->source, $context["locale"], "locale", [], "any", false, false, false, 148))), "html", null, true);
            echo "</a>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 150
        echo "                            </div>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>
<!--rv-slider-->
<section class=\"bannercontainer row\">
        <div class=\"video-container\">
                <div class=\"filter\"></div>
                <video autoplay=\"\" muted=\"\" loop=\"\" class=\"fillWidth video-mobile-hidden\" poster=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/slider-img/27.JPG"), "html", null, true);
        echo "\">
                    <source src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/videos/Megadrvosnimak.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\">
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                    <source src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/videos/Megadrvosnimak.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\">
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
            </div>
            <div class=\"rev_slider banner row m0 rev-slider-height\" id=\"rev_slider\" data-version=\"5.0\">
                    <ul style='top: 150px'>
                        <li data-transition=\"slidehorizontal\"  data-delay=\"10000\" class=\"text-center\">
                            <div class=\"carpenters-h1\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"255\" data-voffset=\"160\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"900\">
                                ";
        // line 177
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 177, $this->source); })()), "title", [], "any", false, false, false, 177), "html", null, true);
        echo "
            
                            </div>
                            <div class=\"sfb carpenters-h2\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"310\" data-voffset=\"350\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1200\">
                                ";
        // line 185
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 185, $this->source); })()), "subtitle", [], "any", false, false, false, 185), "html", null, true);
        echo "
                            </div>
                            <!-- <div class=\"sfb carpenters-ul w-35\" style='margin: auto'
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"375\" data-voffset=\"470\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1500\">
                                <ul class=\"nav\">
                                    <li><a href=\"#\">Domestic</a></li>
                                    <li><a href=\"#\">Commercial</a></li>
                                    <li><a href=\"#\">Industrial</a></li>
                                </ul>
                            </div> -->
                            <div class=\"sfb carpenters-p head-text\"
                                data-x=\"0\" data-hoffset=\"500\" 
                                data-y=\"430\" data-voffset=\"470\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1800\">                    
";
        // line 204
        echo "                                ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 204, $this->source); })()), "text", [], "any", false, false, false, 204), "html", null, true);
        echo "
                            </div>
                            <div class=\"sfb carpenters-b\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"550\" data-voffset=\"555\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"2000\">                    
                                <a href=\"#\" class=\"btn btn-2 submit\">Contact us</a>
                                <a href=\"#\" class=\"btn btn-2 submit\">our services</a>
                            </div>
                        </li>
                        </ul>
                        </div>
    <!-- <div class=\"rev_slider banner row m0\" id=\"rev_slider\" data-version=\"5.0\">
        <ul>
            <li data-transition=\"slidehorizontal\"  data-delay=\"10000\">
                <img src=\"images/slider-img/bg.jpg\"  alt=\"\" data-bgposition=\"center top\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"1\" >
                <div class=\"tp-caption sfr tp-resizeme carpenters-h1\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"255\" data-voffset=\"160\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"900\">
                    we are available

                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-h2\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"310\" data-voffset=\"350\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1200\">
                    for your wooden work
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-ul\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"375\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1500\">
                    <ul class=\"nav\">
                        <li><a href=\"#\">Domestic</a></li>
                        <li><a href=\"#\">Commercial</a></li>
                        <li><a href=\"#\">Industrial</a></li>
                    </ul>
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-p\" 
                    data-x=\"0\" data-hoffset=\"500\" 
                    data-y=\"430\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1800\">                    
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed<br> do eiusmod tempor incididunt ut labore et dolore magna<br> aliqua. Ut enim ad minim veniam exercitation.
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-b\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"550\" data-voffset=\"555\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"2000\">                    
                    <a href=\"#\" class=\"btn btn-2 submit\">PUrchase now</a>
                    <a href=\"#\" class=\"btn btn-2 submit\">our services</a>
                </div>
            </li> -->
            <!--
            <li data-transition=\"parallaxvertical\">
                <img src=\"images/slider-img/2.jpg\"  alt=\"\" data-bgposition=\"center top\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"1\" >
                
                <div class=\"tp-caption sfb tp-resizeme carpenters-ul type2\" 
                    data-x=\"left\" data-hoffset=\"620\" 
                    data-y=\"255\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1500\">
                    <ul class=\"nav\">
                        <li><a href=\"#\">Domestic</a></li>
                        <li><a href=\"#\">Commercial</a></li>
                        <li><a href=\"#\">Industrial</a></li>
                    </ul>
                </div>
                <div class=\"tp-caption sfr tp-resizeme carpenters-h1 type2\" 
                    data-x=\"left\" data-hoffset=\"620\" 
                    data-y=\"320\" data-voffset=\"160\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"900\">
                    <span>Design</span> futniture<br><span>Make</span> doors
                </div>
            </li>
        </ul> -->
    <!-- </div>  -->
</section>

<!--experiance-area-->
<section class=\"row experience-area\">
   <div class=\"container\">
       <div class=\"row\">
           <div class=\"col-sm-5 worker-image\">
               ";
        // line 295
        $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 295, $this->source); })()), "leftImage", [], "any", false, false, false, 295), "file");
        // line 296
        echo "               <img src=\"";
        echo twig_escape_filter($this->env, (isset($context["imageSrc"]) || array_key_exists("imageSrc", $context) ? $context["imageSrc"] : (function () { throw new RuntimeError('Variable "imageSrc" does not exist.', 296, $this->source); })()), "html", null, true);
        echo "\" alt=\"\">
           </div>
           <div class=\"col-sm-7 experience-info\">
              <div class=\"content\">
                  <h2>";
        // line 300
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 300, $this->source); })()), "title", [], "any", false, false, false, 300), "html", null, true);
        echo "</h2>
                  <p>";
        // line 301
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 301, $this->source); })()), "text", [], "any", false, false, false, 301), "html", null, true);
        echo "</p>
              </div>
              <ul class=\"content-list\">
                  <li><a href=\"#\">";
        // line 304
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 304, $this->source); })()), "item1", [], "any", false, false, false, 304), "html", null, true);
        echo "</a></li>
                  <li><a href=\"#\">";
        // line 305
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 305, $this->source); })()), "item2", [], "any", false, false, false, 305), "html", null, true);
        echo "</a></li>
                  <li><a href=\"#\">";
        // line 306
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 306, $this->source); })()), "item3", [], "any", false, false, false, 306), "html", null, true);
        echo "</a></li>
                  <li><a href=\"#\">";
        // line 307
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 307, $this->source); })()), "item4", [], "any", false, false, false, 307), "html", null, true);
        echo "</a></li>
              </ul>
              <div class=\"content-image\">
                  ";
        // line 310
        $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["experience"]) || array_key_exists("experience", $context) ? $context["experience"] : (function () { throw new RuntimeError('Variable "experience" does not exist.', 310, $this->source); })()), "rightImage", [], "any", false, false, false, 310), "file");
        // line 311
        echo "              <img src=\"";
        echo twig_escape_filter($this->env, (isset($context["imageSrc"]) || array_key_exists("imageSrc", $context) ? $context["imageSrc"] : (function () { throw new RuntimeError('Variable "imageSrc" does not exist.', 311, $this->source); })()), "html", null, true);
        echo "\" alt=\"\">
              </div>
               
           </div>
       </div>
   </div>
</section>


<!--we-do-->
<section class=\"row sectpad we-do-section\">
    <div class=\"container\">
        <div class=\"row m0 section_header color\">
           <h2>What we do</h2> 
        </div>
        <div class=\"we-do-slider\">
            <div class=\"we-sliders\">
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"";
        // line 330
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/we-do/6.png"), "html", null, true);
        echo "\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>HARDWOOD FLOORING</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/we-do/_DSC1890.png"), "html", null, true);
        echo "\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Home Wood Work</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"";
        // line 344
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/we-do/26.png"), "html", null, true);
        echo "\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Indoor Furniture</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"";
        // line 351
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/we-do/28.png"), "html", null, true);
        echo "\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Outdoor Furniture</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Projects -->
<!-- <section class=\"row latest_projects sectpad projects-1\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
            <h2>Our Latest Projects</h2>
        </div>
        <div class=\"row m0 filter_row\">
            <ul class=\"nav project_filter\" id=\"project_filter2\">
                <li class=\"active\" data-filter=\"*\">all</li>
                <li data-filter=\".indoor\">indoor furniture</li>
                <li data-filter=\".renovation\">renovation of house</li>
                <li data-filter=\".hardwood\">hardwood flooring</li>
                <li data-filter=\".wood_supply\">wood supply</li>
                <li data-filter=\".manufacturing\">furniture manufacturing</li>
                <li data-filter=\".repairing\">repairing</li>
            </ul>
        </div>
        <div class=\"projects2 popup-gallery\" id=\"projects\">
            <div class=\"grid-sizer\"></div>
            <div class=\"col-sm-6 col-xs-6 project indoor wood_supply\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/1.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/1.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project indoor hardwood renovation renovation\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/2.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/2.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project indoor manufacturing\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/3.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/3.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project manufacturing repairing\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/4.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/4.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project repairing renovation\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/5.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/5.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-6 col-xs-6 project indoor wood_supply\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/6.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/6.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--work-shop-->
<section class=\"row fluid-work-area\">
    <div class=\"work-image\">
        <img src=\"";
        // line 477
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/workshop/33.png"), "html", null, true);
        echo "\" alt=\"\">
    </div>
    <div class=\"work-promo\">
        <div class=\"promo-content\">
            <h2>Welcome to Mega drvo</h2>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quae rat voluptatem. Ut enim ad minima.</p>
            <h3>We are available for 24/7 for you requirements</h3>
            <ul class=\"nav\">
                <li>Complete Savety Analysis</li>
                <li>Complete Savety Analysis</li>
                <li>Certified Company Since 2005</li>
                <li>Certified Company Since 2005</li>
            </ul>
        </div>
    </div>
</section>
<!--testimonial-->
<section class=\"row sectpad testimonial-area\">
   <div class=\"container\">
       <div class=\"row m0 section_header common\">
           <h2>What our client says</h2> 
        </div>
        <div class=\"testimonial-sliders\">
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"";
        // line 504
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/testimonial/testemonial_1.jpg"), "html", null, true);
        echo "\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  Martin Lock</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"";
        // line 517
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/testimonial/testimonials_0.jpg"), "html", null, true);
        echo "\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  John Michale</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"";
        // line 530
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/testimonial/testimonial_2.jpg"), "html", null, true);
        echo "\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  Richard Webster</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"";
        // line 543
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/testimonial/testimonial_3.jpg"), "html", null, true);
        echo "\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">- Edward Dink</a>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>

<!-- latest-news-area -->
<section class=\"row sectpad latest-news-area\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
           <h2>latest news</h2> 
        </div>
        <div class=\"row latest-content\">
            <div class=\"col-sm-4 clo-xs-12 latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"";
        // line 565
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/blogs/4.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>14 <small>FEB</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                        <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>    
            <div class=\"col-sm-4 clo-xs-12  latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"";
        // line 580
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/blogs/8.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>24 <small>MAY</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog-details.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                       <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-4 clo-xs-12 latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"";
        // line 595
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/blogs/9.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>03 <small>SEP</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                        <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- clients -->
<!-- <section class=\"row clients\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
            <h2>Our Clients</h2>
        </div>
        <div class=\"row clients-logos\">
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/1.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/2.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/3.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/4.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"blog-details.html\"><img src=\"images/clients-logo/5.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"blog-details.html\"><img src=\"images/clients-logo/6.png\" alt=\"\"></a>
                </div>
            </div>
        </div>
    </div>
</section> -->

<!--great-work-->
<section class=\"emergency-contact\">
    <div class=\"left-side\">
        <div class=\"content\">
            <h3>If you have any wood work  need...</h3>
            <h3>Simply call our 24 hour emergecny number.</h3>
            <a href=\"tel:+018655248503\" class=\"phone\"><img src=\"";
        // line 660
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/great-work/phone.png"), "html", null, true);
        echo "\" alt=\"\">+387 55 256 032</a>
            <a href=\"tel:+018655248503\" class=\"email\"><img src=\"";
        // line 661
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/great-work/email.png"), "html", null, true);
        echo "\" alt=\"\">megadrvo@gmail.com</a>
        </div>
    </div>
    <div class=\"right-side\" style=\"height: 213px; overflow: hidden;\">
        <img src=\"";
        // line 665
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/expreence//14.png"), "html", null, true);
        echo "\" style=\"width: 100%;\" alt=\"\">
    </div>
</section>

<!--footer-->
<footer class=\"row\">
    <div class=\"row m0 footer-top\">
        <div class=\"container\">
            <div class=\"row footer-sidebar\">
                <div class=\"widget about-us-widget col-sm-6 col-lg-3\">
                    <a href=\"index.html.twig\" class=\"brand_logo\">
                        <img src=\"";
        // line 676
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/images/clients-logo/18a.png"), "html", null, true);
        echo "\" class=\"w-50\" alt=\"logo image\">
                    </a>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    <div class=\"social-icon row m0\">
                        <ul class=\"nav\">
                            <li><a href=\"#\"><i class=\"fa fa-facebook-square\"></i></a></li>
                            <li><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget-links col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">our services</h4>
                    <div class=\"widget-contact-list row m0\">
                        <ul>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden boards</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden beams</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden kant</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Make Quality Products</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget-contact  col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">Get in Touch</h4>
                    <div class=\"widget-contact-list row m0\">
                       <ul>
                            <li>
                                <i class=\"fa fa-map-marker\"></i>
                                <div class=\"fleft location_address\">
                                    Brčanska cesta bb, 76329 Velika Obarska
                                </div>
                                
                            </li>
                            <li>
                                <i class=\"fa fa-phone\"></i>
                                <div class=\"fleft contact_no\">
                                    <a href=\"#\">+387 55 255 188</a>
                                </div>
                            </li>
                            <li>
                                <i class=\"fa fa-envelope-o\"></i>
                                <div class=\"fleft contact_mail\">
                                    <a href=\"#\">www.megadrvo@gmail.com</a>
                                </div>
                            </li>
                            <li>
                                <i class=\"icon icon-WorldWide\"></i>
                                <div class=\"fleft service_time\">
                                    Mon - Sun : 7am to 5pm
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget4 widget-form col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">Drop a message</h4>
                    <div class=\"widget-contact-list row m0\">
                        <form class=\"submet-form row m0\" action=\"#\" method=\"post\">
                            <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Name\">
                            <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email Address\">
                            <textarea class=\"form-control message\" placeholder=\"Message\"></textarea>
                            <button class=\"submit\" type=\"submit\">submit now</button>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
     </div>
     <div class=\"row m0 footer-bottom\">
         <div class=\"container\">
            <div class=\"row\">
               <div class=\"col-sm-8\">
                   Copyright &copy; <a href=\"index.html.twig\">Mega Drvo</a> 2019. <br class=\"visible-xs\"> All rights reserved.
               </div>
            </div>
        </div>
     </div>
</footer>

   
";
        // line 770
        echo "
";
        // line 778
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "site/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  881 => 778,  878 => 770,  795 => 676,  781 => 665,  774 => 661,  770 => 660,  702 => 595,  684 => 580,  666 => 565,  641 => 543,  625 => 530,  609 => 517,  593 => 504,  563 => 477,  434 => 351,  424 => 344,  414 => 337,  404 => 330,  381 => 311,  379 => 310,  373 => 307,  369 => 306,  365 => 305,  361 => 304,  355 => 301,  351 => 300,  343 => 296,  341 => 295,  246 => 204,  225 => 185,  214 => 177,  199 => 165,  194 => 163,  190 => 162,  176 => 150,  166 => 148,  160 => 147,  156 => 146,  149 => 142,  146 => 141,  144 => 140,  79 => 78,  59 => 60,  52 => 59,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{#<!DOCTYPE html>#}
{#<html lang=\"en\">#}
{#  <head>#}
{#    <meta charset=\"utf-8\">#}
{#    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">#}
{#    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">#}
{#    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->#}
{#    <title>Mega Drvo doo</title>#}
{#    #}
{#    <!--Favicon-->#}
{#    <link rel=\"icon\" href=\"favicon/mega-drvo-72x72.png\" type=\"image/gif\" sizes=\"36x36\">#}
{#    <!-- <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"favicon/apple-icon-60x60.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"favicon/apple-icon-72x72.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"favicon/apple-icon-76x76.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"favicon/apple-icon-114x114.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"favicon/apple-icon-120x120.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"favicon/apple-icon-144x144.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"favicon/apple-icon-152x152.png\">#}
{#    <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"favicon/apple-icon-180x180.png\">#}
{#    <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"favicon/android-icon-192x192.png\">#}
{#    <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"favicon/favicon-32x32.png\">#}
{#    <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"favicon/favicon-96x96.png\">#}
{#    <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"favicon/favicon-16x16.png\">#}
{#    <link rel=\"manifest\" href=\"favicon/manifest.json\">#}
{#    <meta name=\"msapplication-TileColor\" content=\"#ffffff\">#}
{#    <meta name=\"msapplication-TileImage\" content=\"/ms-icon-144x144.png\">#}
{#    <meta name=\"theme-color\" content=\"#ffffff\"> -->#}

{#   <!--    fonts-->#}
{#    <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,700,500,400,600' rel='stylesheet' type='text/css'>#}
{#    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,300italic' rel='stylesheet' type='text/css'>#}
{#    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700' rel='stylesheet' type='text/css'>#}
{#    #}
{#    <link href='https://fonts.googleapis.com/css?family=Alegreya:400,700,700italic,400italic' rel='stylesheet' type='text/css'>#}
{#    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'> -->#}
{#   #}
{#    <!-- Bootstrap -->#}
{#    <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">#}
{#    <link href=\"css/bootstrap-theme.min.css\" rel=\"stylesheet\">#}
{#    <link href=\"css/font-awesome.min.css\" rel=\"stylesheet\">#}
{#    <link href=\"css/strock-icon.css\" rel=\"stylesheet\">#}
{#    <!--    owl-carousel-->#}
{#    <link rel=\"stylesheet\" href=\"vendors/owlcarousel/owl.carousel.css\"> #}
{#    <link href=\"vendors/rs-plugin/css/settings.css\" rel=\"stylesheet\">#}
{#    <link href=\"vendors/magnific/magnific-popup.css\" rel=\"stylesheet\">#}
{#    <!--    css-->#}
{#    <link rel=\"stylesheet\" href=\"css/style.css\">#}
{#    #}
{#    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->#}
{#    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->#}
{#    <!--[if lt IE 9]>#}
{#      <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>#}
{#      <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>#}
{#    <![endif]-->#}
{#</head>#}
{#<body>#}
{% block body %}

<header class=\"row header navbar-static-top\" id=\"main_navbar\">
    <div class=\"container\">
        <div class=\"row m0 social-info\">
            <ul class=\"social-icon\">
                <li><a href=\"https://www.facebook.com/doomegadrvo/\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>
                <!-- <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li> -->
                <!-- <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li> -->
                <li><a href=\"https://www.instagram.com/megadrvo/\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>
                <li class=\"tel\"><a href=\"tel:+1234567890\"><i class=\"fa fa-phone\"></i> +387 55 255 188 </a></li>
                <li class=\"tel\"><a href=\"tel:+1234567890\"><i class=\"fa fa-phone\"></i> +387 55 256 032 </a></li>
                <li class=\"email\"><a href=\"#\"><i class=\"fa fa-envelope-o\"></i> megadrvo@gmail.com</a></li>
            </ul>
        </div>
    </div>
   <div class=\"logo_part\">
        <div class=\"logo\">
            <a href=\"index.html.twig\" class=\"brand_logo md-logo\">
                <img src=\"{{ asset('template/images/header/18a.png')}}\" alt=\"logo image\">
            </a>
        </div>
    </div>
    <div class=\"main-menu\">
        <nav class=\"navbar navbar-default\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#main_nav\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            
            <div class=\"menu row m0\">
                <div class=\"collapse navbar-collapse\" id=\"main_nav\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"index.html.twig\">Home</a></li>
                        <li><a href=\"#\">About Us</a></li>
                        <li><a href=\"#\">Services</a></li>
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">services</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"service.html\">services</a></li>
                                <li><a href=\"services-2.html\">services 2</a></li>
                            </ul>
                        </li> -->
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Our Products</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"#\">product</a></li>
                                <li><a href=\"#\">product 2</a></li>
                                <li><a href=\"#\">product 3</a></li>
                                <li><a href=\"#\">product details</a></li>
                            </ul>
                        </li>
                        <li><a href=\"#\">team</a></li>
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">blog</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"blog.html\">blog</a></li>
                                <li><a href=\"blog-details.html\">blog details</a></li>
                            </ul>
                        </li> -->
                        <!-- <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Shop</a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"shop.html\">Shop</a></li>
                                <li><a href=\"product.html\">Product Details</a></li>
                                <li><a href=\"cart.html\">Cart Page</a></li>
                                <li><a href=\"checkout.html\">Checkout</a></li>
                            </ul>
                        </li> -->
                        <li><a href=\"#\">COR</a></li>
                        <li><a href=\"#\">contact us</a></li>
                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle text-white\" href=\"#\" id=\"navbarDropdown\" role=\"button\"
                               data-toggle=\"dropdown\"
                               aria-haspopup=\"true\"
                               aria-expanded=\"false\">

                                {% set loc =  app.request.getLocale() %}

                                {{ ('locale_switcher.' ~ loc) | trans }}
                            </a>
                            <div class=\"dropdown-menu dropdown-menu-right animated bounceInDown\"
                                 aria-labelledby=\"navbarDropdown2\">
                                {% for locale in locales %}
                                    <a class=\"dropdown-item language\" href=\"{{ path('homepage') }}{{ locale.link }}\"
                                       title=\"{{ locale.locale_current_language }}\"> {{ ('locale_switcher.' ~ locale.locale) | trans }}</a>
                                {% endfor %}
                            </div>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>
<!--rv-slider-->
<section class=\"bannercontainer row\">
        <div class=\"video-container\">
                <div class=\"filter\"></div>
                <video autoplay=\"\" muted=\"\" loop=\"\" class=\"fillWidth video-mobile-hidden\" poster=\"{{ asset('template/images/slider-img/27.JPG')}}\">
                    <source src=\"{{ asset('template/videos/Megadrvosnimak.mp4')}}\" type=\"video/mp4\">
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                    <source src=\"{{ asset('template/videos/Megadrvosnimak.mp4')}}\" type=\"video/mp4\">
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
            </div>
            <div class=\"rev_slider banner row m0 rev-slider-height\" id=\"rev_slider\" data-version=\"5.0\">
                    <ul style='top: 150px'>
                        <li data-transition=\"slidehorizontal\"  data-delay=\"10000\" class=\"text-center\">
                            <div class=\"carpenters-h1\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"255\" data-voffset=\"160\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"900\">
                                {{ header.title }}
            
                            </div>
                            <div class=\"sfb carpenters-h2\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"310\" data-voffset=\"350\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1200\">
                                {{ header.subtitle }}
                            </div>
                            <!-- <div class=\"sfb carpenters-ul w-35\" style='margin: auto'
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"375\" data-voffset=\"470\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1500\">
                                <ul class=\"nav\">
                                    <li><a href=\"#\">Domestic</a></li>
                                    <li><a href=\"#\">Commercial</a></li>
                                    <li><a href=\"#\">Industrial</a></li>
                                </ul>
                            </div> -->
                            <div class=\"sfb carpenters-p head-text\"
                                data-x=\"0\" data-hoffset=\"500\" 
                                data-y=\"430\" data-voffset=\"470\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"1800\">                    
{#                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed<br> do eiusmod tempor incididunt ut labore et dolore magna<br> aliqua. Ut enim ad minim veniam exercitation.#}
                                {{ header.text }}
                            </div>
                            <div class=\"sfb carpenters-b\"
                                data-x=\"0\" data-hoffset=\"690\" 
                                data-y=\"550\" data-voffset=\"555\" 
                                data-whitespace=\"nowrap\"
                                data-start=\"2000\">                    
                                <a href=\"#\" class=\"btn btn-2 submit\">Contact us</a>
                                <a href=\"#\" class=\"btn btn-2 submit\">our services</a>
                            </div>
                        </li>
                        </ul>
                        </div>
    <!-- <div class=\"rev_slider banner row m0\" id=\"rev_slider\" data-version=\"5.0\">
        <ul>
            <li data-transition=\"slidehorizontal\"  data-delay=\"10000\">
                <img src=\"images/slider-img/bg.jpg\"  alt=\"\" data-bgposition=\"center top\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"1\" >
                <div class=\"tp-caption sfr tp-resizeme carpenters-h1\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"255\" data-voffset=\"160\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"900\">
                    we are available

                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-h2\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"310\" data-voffset=\"350\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1200\">
                    for your wooden work
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-ul\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"375\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1500\">
                    <ul class=\"nav\">
                        <li><a href=\"#\">Domestic</a></li>
                        <li><a href=\"#\">Commercial</a></li>
                        <li><a href=\"#\">Industrial</a></li>
                    </ul>
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-p\" 
                    data-x=\"0\" data-hoffset=\"500\" 
                    data-y=\"430\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1800\">                    
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed<br> do eiusmod tempor incididunt ut labore et dolore magna<br> aliqua. Ut enim ad minim veniam exercitation.
                </div>
                <div class=\"tp-caption sfb tp-resizeme carpenters-b\" 
                    data-x=\"0\" data-hoffset=\"690\" 
                    data-y=\"550\" data-voffset=\"555\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"2000\">                    
                    <a href=\"#\" class=\"btn btn-2 submit\">PUrchase now</a>
                    <a href=\"#\" class=\"btn btn-2 submit\">our services</a>
                </div>
            </li> -->
            <!--
            <li data-transition=\"parallaxvertical\">
                <img src=\"images/slider-img/2.jpg\"  alt=\"\" data-bgposition=\"center top\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"1\" >
                
                <div class=\"tp-caption sfb tp-resizeme carpenters-ul type2\" 
                    data-x=\"left\" data-hoffset=\"620\" 
                    data-y=\"255\" data-voffset=\"470\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"1500\">
                    <ul class=\"nav\">
                        <li><a href=\"#\">Domestic</a></li>
                        <li><a href=\"#\">Commercial</a></li>
                        <li><a href=\"#\">Industrial</a></li>
                    </ul>
                </div>
                <div class=\"tp-caption sfr tp-resizeme carpenters-h1 type2\" 
                    data-x=\"left\" data-hoffset=\"620\" 
                    data-y=\"320\" data-voffset=\"160\" 
                    data-whitespace=\"nowrap\"
                    data-start=\"900\">
                    <span>Design</span> futniture<br><span>Make</span> doors
                </div>
            </li>
        </ul> -->
    <!-- </div>  -->
</section>

<!--experiance-area-->
<section class=\"row experience-area\">
   <div class=\"container\">
       <div class=\"row\">
           <div class=\"col-sm-5 worker-image\">
               {% set imageSrc = vich_uploader_asset(experience.leftImage, 'file') %}
               <img src=\"{{ imageSrc }}\" alt=\"\">
           </div>
           <div class=\"col-sm-7 experience-info\">
              <div class=\"content\">
                  <h2>{{ experience.title }}</h2>
                  <p>{{ experience.text }}</p>
              </div>
              <ul class=\"content-list\">
                  <li><a href=\"#\">{{ experience.item1 }}</a></li>
                  <li><a href=\"#\">{{ experience.item2 }}</a></li>
                  <li><a href=\"#\">{{ experience.item3 }}</a></li>
                  <li><a href=\"#\">{{ experience.item4 }}</a></li>
              </ul>
              <div class=\"content-image\">
                  {% set imageSrc = vich_uploader_asset(experience.rightImage, 'file') %}
              <img src=\"{{ imageSrc }}\" alt=\"\">
              </div>
               
           </div>
       </div>
   </div>
</section>


<!--we-do-->
<section class=\"row sectpad we-do-section\">
    <div class=\"container\">
        <div class=\"row m0 section_header color\">
           <h2>What we do</h2> 
        </div>
        <div class=\"we-do-slider\">
            <div class=\"we-sliders\">
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"{{ asset('template/images/we-do/6.png')}}\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>HARDWOOD FLOORING</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"{{ asset('template/images/we-do/_DSC1890.png')}}\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Home Wood Work</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"{{ asset('template/images/we-do/26.png')}}\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Indoor Furniture</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
                <div class=\"item\">
                    <div class=\"post-image\">
                        <img src=\"{{ asset('template/images/we-do/28.png')}}\"  alt=\"\">
                    </div>
                    <a href=\"#\"><h4>Outdoor Furniture</h4></a>
                    <p>Lorem ipsum dolor sit amet, cons ecte tur elit. Vestibulum nec odios suspe ndisse ipsum dolor sit.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Projects -->
<!-- <section class=\"row latest_projects sectpad projects-1\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
            <h2>Our Latest Projects</h2>
        </div>
        <div class=\"row m0 filter_row\">
            <ul class=\"nav project_filter\" id=\"project_filter2\">
                <li class=\"active\" data-filter=\"*\">all</li>
                <li data-filter=\".indoor\">indoor furniture</li>
                <li data-filter=\".renovation\">renovation of house</li>
                <li data-filter=\".hardwood\">hardwood flooring</li>
                <li data-filter=\".wood_supply\">wood supply</li>
                <li data-filter=\".manufacturing\">furniture manufacturing</li>
                <li data-filter=\".repairing\">repairing</li>
            </ul>
        </div>
        <div class=\"projects2 popup-gallery\" id=\"projects\">
            <div class=\"grid-sizer\"></div>
            <div class=\"col-sm-6 col-xs-6 project indoor wood_supply\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/1.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/1.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project indoor hardwood renovation renovation\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/2.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/2.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project indoor manufacturing\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/3.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/3.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project manufacturing repairing\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/4.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/4.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-3 col-xs-6 project repairing renovation\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/5.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/5.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-6 col-xs-6 project indoor wood_supply\">
                <div class=\"project-img\">
                    <img src=\"images/latest-project/6.jpg\" alt=\"\">
                    <div class=\"project-text\">
                        <ul class=\"list-unstyled\">
                            <li><a href=\"projects-details.html\"><i class=\"icon icon-Linked\"></i></a></li>
                            <li><a href=\"images/latest-project/6.jpg\" data-source=\"projects-details.html\" title=\"KITCHKEN RENOVATION\" data-desc=\"Wood Work of Rack\" class=\"popup\"><i class=\"icon icon-Search\"></i></a></li>
                        </ul>
                        <div class=\"row m0\">
                            <a href=\"projects-details.html\"><h3>Kitchken renovation</h3></a>
                            <p>Wood Work of Racks</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--work-shop-->
<section class=\"row fluid-work-area\">
    <div class=\"work-image\">
        <img src=\"{{ asset('template/images/workshop/33.png')}}\" alt=\"\">
    </div>
    <div class=\"work-promo\">
        <div class=\"promo-content\">
            <h2>Welcome to Mega drvo</h2>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quae rat voluptatem. Ut enim ad minima.</p>
            <h3>We are available for 24/7 for you requirements</h3>
            <ul class=\"nav\">
                <li>Complete Savety Analysis</li>
                <li>Complete Savety Analysis</li>
                <li>Certified Company Since 2005</li>
                <li>Certified Company Since 2005</li>
            </ul>
        </div>
    </div>
</section>
<!--testimonial-->
<section class=\"row sectpad testimonial-area\">
   <div class=\"container\">
       <div class=\"row m0 section_header common\">
           <h2>What our client says</h2> 
        </div>
        <div class=\"testimonial-sliders\">
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"{{ asset('template/images/testimonial/testemonial_1.jpg')}}\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  Martin Lock</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"{{ asset('template/images/testimonial/testimonials_0.jpg')}}\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  John Michale</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"{{ asset('template/images/testimonial/testimonial_2.jpg')}}\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">-  Richard Webster</a>
                    </div>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"media testimonial\">
                    <div class=\"media-left\">
                        <a href=\"#\">
                            <img src=\"{{ asset('template/images/testimonial/testimonial_3.jpg')}}\"  alt=\"\">
                        </a>
                    </div>
                    <div class=\"media-body\">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href=\"#\">- Edward Dink</a>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>

<!-- latest-news-area -->
<section class=\"row sectpad latest-news-area\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
           <h2>latest news</h2> 
        </div>
        <div class=\"row latest-content\">
            <div class=\"col-sm-4 clo-xs-12 latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"{{ asset('template/images/blogs/4.png')}}\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>14 <small>FEB</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                        <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>    
            <div class=\"col-sm-4 clo-xs-12  latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"{{ asset('template/images/blogs/8.png')}}\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>24 <small>MAY</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog-details.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                       <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-4 clo-xs-12 latest\">
                <div class=\"row m0 latest-image\">
                    <a href=\"blog-details.html\"><img src=\"{{ asset('template/images/blogs/9.png')}}\" alt=\"\"></a>
                    <div class=\"latest-info-date\"><a href=\"#\"><h3>03 <small>SEP</small></h3></a></div>
                </div>
                <div class=\"latest-news-text\">
                    <a href=\"blog.html\">
                        <h4>Wood Work Adds Value to Your Property</h4>
                    </a>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
                    <div class=\"row m0 latest-meta\">
                        <a href=\"#\"><i class=\"fa fa-user\"></i>Anjori Meyami</a> <a class=\"read_more\" href=\"single.html\"><i class=\"fa fa-comments\"></i> Comments: 6</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- clients -->
<!-- <section class=\"row clients\">
    <div class=\"container\">
        <div class=\"row m0 section_header\">
            <h2>Our Clients</h2>
        </div>
        <div class=\"row clients-logos\">
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/1.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/2.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/3.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"#\"><img src=\"images/clients-logo/4.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"blog-details.html\"><img src=\"images/clients-logo/5.png\" alt=\"\"></a>
                </div>
            </div>
            <div class=\"col-md-2 col-sm-3 col-xs-6 client\">
                <div class=\"row m0 inner-logo\">
                   <a href=\"blog-details.html\"><img src=\"images/clients-logo/6.png\" alt=\"\"></a>
                </div>
            </div>
        </div>
    </div>
</section> -->

<!--great-work-->
<section class=\"emergency-contact\">
    <div class=\"left-side\">
        <div class=\"content\">
            <h3>If you have any wood work  need...</h3>
            <h3>Simply call our 24 hour emergecny number.</h3>
            <a href=\"tel:+018655248503\" class=\"phone\"><img src=\"{{ asset('template/images/great-work/phone.png')}}\" alt=\"\">+387 55 256 032</a>
            <a href=\"tel:+018655248503\" class=\"email\"><img src=\"{{ asset('template/images/great-work/email.png')}}\" alt=\"\">megadrvo@gmail.com</a>
        </div>
    </div>
    <div class=\"right-side\" style=\"height: 213px; overflow: hidden;\">
        <img src=\"{{ asset('template/images/expreence//14.png')}}\" style=\"width: 100%;\" alt=\"\">
    </div>
</section>

<!--footer-->
<footer class=\"row\">
    <div class=\"row m0 footer-top\">
        <div class=\"container\">
            <div class=\"row footer-sidebar\">
                <div class=\"widget about-us-widget col-sm-6 col-lg-3\">
                    <a href=\"index.html.twig\" class=\"brand_logo\">
                        <img src=\"{{ asset('template/images/clients-logo/18a.png')}}\" class=\"w-50\" alt=\"logo image\">
                    </a>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    <div class=\"social-icon row m0\">
                        <ul class=\"nav\">
                            <li><a href=\"#\"><i class=\"fa fa-facebook-square\"></i></a></li>
                            <li><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget-links col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">our services</h4>
                    <div class=\"widget-contact-list row m0\">
                        <ul>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden boards</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden beams</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Wooden kant</a></li>
                            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i>Make Quality Products</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget-contact  col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">Get in Touch</h4>
                    <div class=\"widget-contact-list row m0\">
                       <ul>
                            <li>
                                <i class=\"fa fa-map-marker\"></i>
                                <div class=\"fleft location_address\">
                                    Brčanska cesta bb, 76329 Velika Obarska
                                </div>
                                
                            </li>
                            <li>
                                <i class=\"fa fa-phone\"></i>
                                <div class=\"fleft contact_no\">
                                    <a href=\"#\">+387 55 255 188</a>
                                </div>
                            </li>
                            <li>
                                <i class=\"fa fa-envelope-o\"></i>
                                <div class=\"fleft contact_mail\">
                                    <a href=\"#\">www.megadrvo@gmail.com</a>
                                </div>
                            </li>
                            <li>
                                <i class=\"icon icon-WorldWide\"></i>
                                <div class=\"fleft service_time\">
                                    Mon - Sun : 7am to 5pm
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"widget widget4 widget-form col-sm-6 col-lg-3\">
                    <h4 class=\"widget_title\">Drop a message</h4>
                    <div class=\"widget-contact-list row m0\">
                        <form class=\"submet-form row m0\" action=\"#\" method=\"post\">
                            <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Name\">
                            <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email Address\">
                            <textarea class=\"form-control message\" placeholder=\"Message\"></textarea>
                            <button class=\"submit\" type=\"submit\">submit now</button>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
     </div>
     <div class=\"row m0 footer-bottom\">
         <div class=\"container\">
            <div class=\"row\">
               <div class=\"col-sm-8\">
                   Copyright &copy; <a href=\"index.html.twig\">Mega Drvo</a> 2019. <br class=\"visible-xs\"> All rights reserved.
               </div>
            </div>
        </div>
     </div>
</footer>

   
{#<script src=\"js/jquery-2.2.0.min.js\"></script>#}
{#<script src=\"js/bootstrap.min.js\"></script>#}
{#<!--RS-->#}
{#<script src=\"vendors/rs-plugin/js/jquery.themepunch.tools.min.js\"></script> <!-- Revolution Slider Tools -->#}
{#<script src=\"vendors/rs-plugin/js/jquery.themepunch.revolution.min.js\"></script> <!-- Revolution Slider -->#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.actions.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.carousel.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.kenburn.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.migration.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.navigation.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.parallax.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.slideanims.min.js\"></script>#}
{#<script src=\"vendors/rs-plugin/js/extensions/revolution.extension.video.min.js\"></script>#}

{#<script src=\"vendors/isotope/isotope.min.js\"></script>#}
{#<script src=\"vendors/imagesloaded/imagesloaded.pkgd.min.js\"></script>#}
{#<script src=\"vendors/owlcarousel/owl.carousel.min.js\"></script>#}
{#<script src=\"vendors/magnific/jquery.magnific-popup.min.js\"></script>#}
{#<script src=\"js/theme.js\"></script>#}
{#</body>#}
{#</html>#}

{% endblock %}", "site/index.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/site/index.html.twig");
    }
}
