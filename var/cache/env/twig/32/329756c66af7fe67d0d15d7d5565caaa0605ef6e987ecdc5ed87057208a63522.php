<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/contact_process.php */
class __TwigTemplate_8341c6266255d13a8052bcb46e9e38d10f8505f4d2e7082c96a3195b00b33a81 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/contact_process.php"));

        // line 1
        echo "<?php

    \$to = \"vinodpal06@rediffmail.com\";
    \$from = \$_REQUEST['yourname'];
    \$name = \$_REQUEST['youremail'];
    \$headers = \"From: \$from\";
    \$subject = \"You have a message from your OnePro.\";

    \$fields = array();
    \$fields{\"yourname\"} = \"name\";
    \$fields{\"youremail\"} = \"email\";
    \$fields{\"subject\"} = \"subject\";
    \$fields{\"message\"} = \"message\";

    \$body = \"Here is what was sent:\\n\\n\"; foreach(\$fields as \$a => \$b){   \$body .= sprintf(\"%20s: %s\\n\",\$b,\$_REQUEST[\$a]); }

    \$send = mail(\$to, \$subject, \$body, \$headers);

?>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "site/contact_process.php";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php

    \$to = \"vinodpal06@rediffmail.com\";
    \$from = \$_REQUEST['yourname'];
    \$name = \$_REQUEST['youremail'];
    \$headers = \"From: \$from\";
    \$subject = \"You have a message from your OnePro.\";

    \$fields = array();
    \$fields{\"yourname\"} = \"name\";
    \$fields{\"youremail\"} = \"email\";
    \$fields{\"subject\"} = \"subject\";
    \$fields{\"message\"} = \"message\";

    \$body = \"Here is what was sent:\\n\\n\"; foreach(\$fields as \$a => \$b){   \$body .= sprintf(\"%20s: %s\\n\",\$b,\$_REQUEST[\$a]); }

    \$send = mail(\$to, \$subject, \$body, \$headers);

?>", "site/contact_process.php", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/site/contact_process.php");
    }
}
