<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/post/list.html.twig */
class __TwigTemplate_ba424e7d884d3fcc68b4c376201c26f8a09b7278156f538a2fe7270d35f528a3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/post/list.html.twig"));

        $this->parent = $this->loadTemplate("admin_base.html.twig", "admin/post/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"col-12 grid-margin stretch-card\">
    <div class=\"card\">
        <div class=\"table-responsive\">
            <table class=\"table table-striped\">
                <thead>
                <tr>
                    <th scope=\"col\">Slika</th>
                    <th scope=\"col\">Detalji</th>
                    <th scope=\"col\">Akcije</th>
                </tr>
                </thead>
                <tbody>
                ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 18
            echo "                    <tr>
                        <td width=\"30%\">

                            ";
            // line 21
            $context["picture"] = twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 21);
            // line 22
            echo "                            ";
            $context["imageSrc"] = $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 22), "file");
            // line 23
            echo "                            <div id=\"item_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture"]) || array_key_exists("picture", $context) ? $context["picture"] : (function () { throw new RuntimeError('Variable "picture" does not exist.', 23, $this->source); })()), "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                                <div class=\"image\">
                                    <img width=\"250\" id=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["picture"]) || array_key_exists("picture", $context) ? $context["picture"] : (function () { throw new RuntimeError('Variable "picture" does not exist.', 25, $this->source); })()), "id", [], "any", false, false, false, 25), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["imageSrc"]) || array_key_exists("imageSrc", $context) ? $context["imageSrc"] : (function () { throw new RuntimeError('Variable "imageSrc" does not exist.', 25, $this->source); })()), "html", null, true);
            echo "\"/>
                                </div>
                            </div>

                        </td>

                        <td width=\"45%\">
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Jezik: ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "language", [], "any", false, false, false, 34), "html", null, true);
            echo "
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Naslov: ";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 39), "html", null, true);
            echo "
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Tekst: ";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "text", [], "any", false, false, false, 44), "html", null, true);
            echo "
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <a class=\"btn btn-default btn-sm\"
                                       href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_post_form", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 52)]), "html", null, true);
            echo "\">
                                        Izmjeni
                                    </a>
                                </div>
                            </div>
                            <br>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <a class=\"btn btn-danger btn-sm\"
                                       href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_post_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 61)]), "html", null, true);
            echo "\">
                                        Izbrisi
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>


                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                </tbody>
            </table>
        </div>
    </div>


    <div class=\"clearfix\"></div>

    <ul class=\"pagination\">
        ";
        // line 80
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 80, $this->source); })()));
        echo "
    </ul>
</div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/post/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 80,  163 => 71,  147 => 61,  135 => 52,  124 => 44,  116 => 39,  108 => 34,  94 => 25,  88 => 23,  85 => 22,  83 => 21,  78 => 18,  74 => 17,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin_base.html.twig' %}

{% block body %}

<div class=\"col-12 grid-margin stretch-card\">
    <div class=\"card\">
        <div class=\"table-responsive\">
            <table class=\"table table-striped\">
                <thead>
                <tr>
                    <th scope=\"col\">Slika</th>
                    <th scope=\"col\">Detalji</th>
                    <th scope=\"col\">Akcije</th>
                </tr>
                </thead>
                <tbody>
                {% for post in posts %}
                    <tr>
                        <td width=\"30%\">

                            {% set picture = post.image %}
                            {% set imageSrc = vich_uploader_asset(post.image, 'file') %}
                            <div id=\"item_{{ picture.id }}\">
                                <div class=\"image\">
                                    <img width=\"250\" id=\"{{ picture.id }}\" src=\"{{ imageSrc }}\"/>
                                </div>
                            </div>

                        </td>

                        <td width=\"45%\">
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Jezik: {{ post.language }}
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Naslov: {{ post.title }}
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    Tekst: {{ post.text }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <a class=\"btn btn-default btn-sm\"
                                       href=\"{{ path('admin_post_form', {id: post.id}) }}\">
                                        Izmjeni
                                    </a>
                                </div>
                            </div>
                            <br>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <a class=\"btn btn-danger btn-sm\"
                                       href=\"{{ path('admin_post_delete', {id: post.id}) }}\">
                                        Izbrisi
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>


                {% endfor %}
                </tbody>
            </table>
        </div>
    </div>


    <div class=\"clearfix\"></div>

    <ul class=\"pagination\">
        {{ knp_pagination_render(posts) }}
    </ul>
</div>


{% endblock %}", "admin/post/list.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/templates/admin/post/list.html.twig");
    }
}
