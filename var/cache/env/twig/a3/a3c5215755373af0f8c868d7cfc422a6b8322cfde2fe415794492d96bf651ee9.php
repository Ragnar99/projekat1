<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* LuneticsLocaleBundle:Switcher:switcher_links.html.twig */
class __TwigTemplate_6c6ae894e6f2d2029b13d12dacdcd95002d787a2345fe31e2f204d2bfb4306f7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "LuneticsLocaleBundle:Switcher:switcher_links.html.twig"));

        // line 1
        echo "<p>Switch to :</p>
<ul class=\"locale_chooser\">
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["locales"]) || array_key_exists("locales", $context) ? $context["locales"] : (function () { throw new RuntimeError('Variable "locales" does not exist.', 3, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 4
            echo "        <li";
            if (((isset($context["current_locale"]) || array_key_exists("current_locale", $context) ? $context["current_locale"] : (function () { throw new RuntimeError('Variable "current_locale" does not exist.', 4, $this->source); })()) == twig_get_attribute($this->env, $this->source, $context["locale"], "locale", [], "any", false, false, false, 4))) {
                echo " class=\"locale_current\"";
            }
            echo ">
            <a href=\"";
            // line 5
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "link", [], "any", false, false, false, 5), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "locale_current_language", [], "any", false, false, false, 5), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["locale"], "locale_current_language", [], "any", false, false, false, 5), "html", null, true);
            echo "</a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "</ul>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "LuneticsLocaleBundle:Switcher:switcher_links.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 8,  55 => 5,  48 => 4,  44 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p>Switch to :</p>
<ul class=\"locale_chooser\">
    {% for locale in locales %}
        <li{% if current_locale == locale.locale %} class=\"locale_current\"{% endif %}>
            <a href=\"{{ locale.link }}\" title=\"{{ locale.locale_current_language }}\">{{ locale.locale_current_language }}</a>
        </li>
    {% endfor %}
</ul>
", "LuneticsLocaleBundle:Switcher:switcher_links.html.twig", "/home/stefan/Desktop/mojiProjekti/megaDrvo/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/Resources/views/Switcher/switcher_links.html.twig");
    }
}
