<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'form.type.lunetics_locale' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/form/FormTypeInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/form/AbstractType.php';
include_once $this->targetDirs[3].'/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/Form/Extension/Type/LocaleType.php';
include_once $this->targetDirs[3].'/vendor/symfony/form/ChoiceList/ChoiceListInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/form/ChoiceList/ArrayChoiceList.php';
include_once $this->targetDirs[3].'/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/Form/Extension/ChoiceList/LocaleChoiceList.php';
include_once $this->targetDirs[3].'/vendor/lunetics/locale-bundle/Lunetics/LocaleBundle/LocaleInformation/LocaleInformation.php';

return $this->privates['form.type.lunetics_locale'] = new \Lunetics\LocaleBundle\Form\Extension\Type\LocaleType(new \Lunetics\LocaleBundle\Form\Extension\ChoiceList\LocaleChoiceList(new \Lunetics\LocaleBundle\LocaleInformation\LocaleInformation(($this->privates['lunetics_locale.validator.meta'] ?? $this->getLuneticsLocale_Validator_MetaService()), ($this->privates['lunetics_locale.guesser_manager'] ?? $this->getLuneticsLocale_GuesserManagerService()), ($this->privates['lunetics_locale.allowed_locales_provider'] ?? $this->getLuneticsLocale_AllowedLocalesProviderService())), true, false));
